| Parameter         | Value |
|:-----------------:|:-----:|
| XLEN              | 64    |
| ADDR_BITS         | 32    |
| INST_BITS         | 32    |
| ISSUE_WIDTH       | 2     |
| PAGE_SIZE         | 8192  |
| I_CACHELINE_BITS  | 512   |
| I_CACHE_NUM_WAYS  | 4     |
| I_CACHE_NUM_SETS  | 128   |
| AXI_ADDR_BITS     | 32    |
| AXI_DATA_BITS     | 128   |
| NUM_PHY_REG_INT   | 64    |
| NUM_PHY_REG_FP    | 64    |
| ROB_DEPTH         | 16    |
