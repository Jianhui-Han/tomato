`include "Configs.svh"

module ROB(
  input   logic                             clk,
  input   logic                             rst_n,
  // decode
  output  logic                             decode_ready,
  input   logic                             decode_valid,
  input   logic [`PHY_REGS_B2-1:0]          decode_rd_0,
  input   logic [`DEC_INFO_BITS-1:0]        decode_info_0,
  input   logic [`PHY_REGS_B2-1:0]          decode_rd_1,
  input   logic [`DEC_INFO_BITS-1:0]        decode_info_1,
  input   logic [`ADDR_BITS-1:0]            decode_pc,
  // issue
  output  logic [`ROB_DEPTH_B2:0]           issue_tag_0,
  output  logic [`ROB_DEPTH_B2:0]           issue_tag_1,
  // commit
  output  logic                             commit_valid_0,
  output  logic [`PHY_REGS_B2-1:0]          commit_stale_0,
  output  logic                             commit_valid_1,
  output  logic [`PHY_REGS_B2-1:0]          commit_stale_1,
  // writeback
  input   logic                             writeback_valid_alu,
  input   logic [`ROB_DEPTH_B2:0]           writeback_tag_alu,
  input   logic                             writeback_valid_lsu,
  input   logic [`ROB_DEPTH_B2:0]           writeback_tag_lsu
);

  logic [`ROB_DEPTH-1:0][`ROB_WIDTH-1:0]    metadata_0;
  logic [`ROB_DEPTH-1:0][`ROB_WIDTH-1:0]    metadata_1;
  logic [`ROB_DEPTH-1:0][`ADDR_BITS-1:0]    metadata_pc;

  logic [`ROB_DEPTH_B2:0]                   rob_head, rob_tail;
  logic                                     committed;

  assign decode_ready = ~((rob_head[`ROB_DEPTH_B2] != rob_tail[`ROB_DEPTH_B2])
      && (rob_head[`ROB_DEPTH_B2-1:0] == rob_tail[`ROB_DEPTH_B2-1:0]));
  assign commit_valid_0 =
      metadata_0[rob_head[`ROB_DEPTH_B2-1:0]][`ROB_WIDTH-2] == 1'b0;
  assign commit_stale_0 =
      metadata_0[rob_head[`ROB_DEPTH_B2-1:0]][`PHY_REGS_B2-1:0];
  assign commit_valid_1 =
      metadata_1[rob_head[`ROB_DEPTH_B2-1:0]][`ROB_WIDTH-2] == 1'b0;
  assign commit_stale_1 =
      metadata_1[rob_head[`ROB_DEPTH_B2-1:0]][`PHY_REGS_B2-1:0];

  always @ (posedge clk or negedge rst_n) begin
    if (!rst_n) begin
      for (int i=0; i<`ROB_DEPTH; i=i+1) begin
        metadata_0[i] <= {(`ROB_WIDTH){1'b0}};
        metadata_1[i] <= {(`ROB_WIDTH){1'b0}};
        metadata_pc[i] <= {(`ADDR_BITS){1'b0}};
        rob_tail <= {(`ROB_DEPTH_B2+1){1'b0}};
        issue_tag_0 <= {(`ROB_DEPTH_B2+1){1'b0}};
        issue_tag_1 <= {(`ROB_DEPTH_B2+1){1'b0}};

        rob_head <= {(`ROB_DEPTH_B2+1){1'b0}};
        committed <= 1'b0;
      end
    end
    else begin
      // dispatch
      if (decode_ready && decode_valid) begin
        metadata_0[rob_tail[`ROB_DEPTH_B2-1:0]] <= {2'b11, decode_rd_0};
        metadata_1[rob_tail[`ROB_DEPTH_B2-1:0]] <= {2'b11, decode_rd_1};
        metadata_pc[rob_tail[`ROB_DEPTH_B2-1:0]] <= decode_pc;
        rob_tail <= rob_tail + 1;
        issue_tag_0 <= {1'b0, rob_tail[`ROB_DEPTH_B2-1:0]};
        issue_tag_1 <= {1'b1, rob_tail[`ROB_DEPTH_B2-1:0]};
      end
      // commit
      if (commit_valid_0 && commit_valid_1) begin
        rob_head <= rob_head + 1;
        metadata_0[rob_head[`ROB_DEPTH_B2-1:0]][`ROB_WIDTH-1:ROB_WIDTH-2] <=
            2'b00;
        metadata_1[rob_head[`ROB_DEPTH_B2-1:0]][`ROB_WIDTH-1:ROB_WIDTH-2] <=
            2'b00;
      end
      else if (commit_valid_0) begin
        committed <= 1'b1;
        metadata_0[rob_head[`ROB_DEPTH_B2-1:0]][`ROB_WIDTH-1:ROB_WIDTH-2] <=
            2'b00;
      end
      else if (commit_valid_1 && committed) begin
        rob_head <= rob_head + 1;
        committed <= 1'b0;
        metadata_1[rob_head[`ROB_DEPTH_B2-1:0]][`ROB_WIDTH-1:ROB_WIDTH-2] <=
            2'b00;
      end
      // writeback
      if (writeback_valid_alu) begin
        if (writeback_tag_alu[`ROB_DEPTH_B2])
          meta_data_1[writeback_tag_alu[`ROB_DEPTH_B2-1:0]][`ROB_WIDTH-2] <=
              1'b0;
        else
          meta_data_0[writeback_tag_alu[`ROB_DEPTH_B2-1:0]][`ROB_WIDTH-2] <=
              1'b0;
      end
      if (writeback_valid_lsu) begin
        if (writeback_tag_lsu[`ROB_DEPTH_B2])
          meta_data_1[writeback_tag_lsu[`ROB_DEPTH_B2-1:0]][`ROB_WIDTH-2] <=
              1'b0;
        else
          meta_data_0[writeback_tag_lsu[`ROB_DEPTH_B2-1:0]][`ROB_WIDTH-2] <=
              1'b0;
      end
    end
  end

endmodule
