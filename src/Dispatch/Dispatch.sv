`include "Config.svh"

module Dispatch(
  input   logic                             clk,
  input   logic                             rst_n,
  // decode
  output  logic                             decode_ready,
  input   logic                             decode_valid,
  input   logic [`PHY_REGS_B2-1:0]          decode_rd_0,
  input   logic [`PHY_REGS_B2-1:0]          decode_rs1_0,
  input   logic [`PHY_REGS_B2-1:0]          decode_rs2_0,
  input   logic [`DEC_INFO_BITS-1:0]        decode_info_0,
  input   logic                             decode_type_0,
  input   logic [`PHY_REGS_B2-1:0]          decode_rd_1,
  input   logic [`PHY_REGS_B2-1:0]          decode_rs1_1,
  input   logic [`PHY_REGS_B2-1:0]          decode_rs2_1,
  input   logic [`DEC_INFO_BITS-1:0]        decode_info_1,
  input   logic                             decode_type_1,
  // issue
  input   logic [1:0]                       issue_ready_alu,
  output  logic [1:0]                       issue_valid_alu,
  input   logic [1:0]                       issue_ready_lsu,
  output  logic [1:0]                       issue_valid_lsu,
  output  logic [`PHY_REGS_B2-1:0]          issue_rd_0,
  output  logic [`PHY_REGS_B2-1:0]          issue_rs1_0,
  output  logic [`PHY_REGS_B2-1:0]          issue_rs2_0,
  output  logic [`DEC_INFO_BITS-1:0]        issue_info_0,
  output  logic [`PHY_REGS_B2-1:0]          issue_rd_1,
  output  logic [`PHY_REGS_B2-1:0]          issue_rs1_1,
  output  logic [`PHY_REGS_B2-1:0]          issue_rs2_1,
  output  logic [`DEC_INFO_BITS-1:0]        issue_info_1
);

  always @ (posedge clk or negedge rst_n) begin
    if (!rst_n) begin
      decode_ready <= 1'b1;
      issue_valid_alu <= 2'b00;
      issue_valid_lsu <= 2'b00; 
    end
    else begin
      case({decode_type_1, decode_type_0})
        2'b00: begin
          if (issue_ready_alu == 2'b11) begin
            decode_ready <= 1'b1;
            issue_valid_alu <= 2'b11;
            issue_valid_lsu <= 2'b00;  
          end
          else
            decode_ready <= 1'b0;
            issue_valid_alu <= 2'b00;
            issue_valid_lsu <= 2'b00;
          end
        end
        2'b01, 2'b10: begin
          if (issue_ready_alu != 0 && issue_ready_lsu != 0) begin
            decode_ready <= 1'b1;
            issue_valid_alu <= {decode_type_1, decode_type_0};
            issue_valid_lsu <= {decode_type_0, decode_type_1};
          end
          else begin
            decode_ready <= 1'b0;
            issue_valid_alu <= 2'b00;
            issue_valid_lsu <= 2'b00;
          end
        end
        2'b11: begin
          if (issue_ready_lsu == 2'b11) begin
            decode_ready <= 1'b1;
            issue_valid_alu <= 2'b00;
            issue_valid_lsu <= 2'b11;
          end
          else begin
            decode_ready <= 1'b0;
            issue_valid_alu <= 2'b00;
            issue_valid_lsu <= 2'b00;
          end
        end
      
      endcase
    end
  end

  always @ (posedge clk or negedge rst_n) begin
    if (!rst_n) begin
      issue_rd_0 <= {(`PHY_REGS_B2){1'b0}};
      issue_rs1_0 <= {(`PHY_REGS_B2){1'b0}};
      issue_rs2_0 <= {(`PHY_REGS_B2){1'b0}};
      issue_info_0 <= {(`DEC_INFO_BITS){1'b0}};
      issue_rd_1 <= {(`PHY_REGS_B2){1'b0}};
      issue_rs1_1 <= {(`PHY_REGS_B2){1'b0}};
      issue_rs2_1 <= {(`PHY_REGS_B2){1'b0}};
      issue_info_1 <= {(`DEC_INFO_BITS){1'b0}};
    end
    else begin
      issue_rd_0 <= decode_rd_0;
      issue_rs1_0 <= decode_rs1_0;
      issue_rs2_0 <= decode_rs2_0;
      issue_info_0 <= decode_info_0;
      issue_rd_1 <= decode_rd_1;
      issue_rs1_1 <= decode_rs1_1;
      issue_rs2_1 <= decode_rs2_1;
      issue_info_1 <= decode_info_1;
    end
  end

endmodule
