`include "Configs.svh"

module ALU32(
  input   logic [31:0]                      operand_a,
  input   logic [31:0]                      operand_b,
  output  logic [31:0]                      result,
  input   logic [3:0]                       opcode
);

  logic [31:0]                              mask;

  assign mask = {32{1'b1}} >> operand_b;
  
  always @ (*) begin
    case (opcode)
      `OPCODE_ADD:  result = operand_a + operand_b;
      `OPCODE_SUB:  result = operand_a - operand_b;
      `OPCODE_AND:  result = operand_a & operand_b;
      `OPCODE_OR:   result = operand_a | operand_b;
      `OPCODE_XOR:  result = operand_a ^ operand_b;
      `OPCODE_SLL:  result = operand_a << operand_b;
      `OPCODE_SRL:  result = operand_a >> operand_b;
      `OPCODE_SRA:  result = (operand_a >> operand_b) & mask | 
          ({64{operand_a[31]}} & (~mask));
      `OPCODE_SLT:  result = operand_a < operand_b ? 32'b1 : 32'b0;
      default:      result = 32'b0;
    endcase
  end

endmodule
