`include "Configs.svh"

module ALU(
  input   logic [`XLEN-1:0]                 operand_a,
  input   logic [`XLEN-1:0]                 operand_b,
  output  logic [`XLEN-1:0]                 result,
  input   logic [3:0]                       opcode,
  input   logic                             word,
  output  logic                             zero
);

  logic [63:0]                              result64;
  logic [31:0]                              resuslt32;

  ALU64 alu64(
    .operand_a  (operand_a  ),
    .operand_b  (operand_b  ),
    .result     (result     ),
    .opcode     (opcode     )
  );

  ALU32 alu32(
    .operand_a  (operand_a[31:0]    ),
    .operand_b  (operand_b[31:0]    ),
    .result     (resuslt32          ),
    .opcode     (opcode             )
  );

  assign result = word ? result64 : {{32'b0}, result32};
  assign zero = result == 64'b0; 

endmodule
