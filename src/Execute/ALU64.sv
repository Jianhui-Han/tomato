`include "Configs.svh"

module ALU64(
  input   logic [63:0]                      operand_a,
  input   logic [63:0]                      operand_b,
  output  logic [63:0]                      result,
  input   logic [3:0]                       opcode
);

  logic [63:0]                              mask;

  assign mask = {64{1'b1}} >> operand_b;
  
  always @ (*) begin
    case (opcode)
      `OPCODE_ADD:  result = operand_a + operand_b;
      `OPCODE_SUB:  result = operand_a - operand_b;
      `OPCODE_AND:  result = operand_a & operand_b;
      `OPCODE_OR:   result = operand_a | operand_b;
      `OPCODE_XOR:  result = operand_a ^ operand_b;
      `OPCODE_SLL:  result = operand_a << operand_b;
      `OPCODE_SRL:  result = operand_a >> operand_b;
      `OPCODE_SRA:  result = (operand_a >> operand_b) & mask | 
          ({64{operand_a[63]}} & (~mask));
      `OPCODE_SLT:  result = operand_a < operand_b ? 64'b1 : 64'b0;
      default:      result = 64'b0;
    endcase
  end

endmodule
