module PriEncoder64(
  input   logic [63:0]                      in,
  output  logic [5:0]                       out       
);

  logic [15:0]              nets;
  logic [3:0]               segment;

  PriEncoder16LSB U3(in[63:48], nets[15:12]);
  PriEncoder16LSB U2(in[47:32], nets[11:8]);
  PriEncoder16LSB U1(in[31:16], nets[7:4]);
  PriEncoder16LSB U0(in[15:0], nets[3:0]);

  assign segment[3] = | in[63:48];
  assign segment[2] = | in[47:32];
  assign segment[1] = | in[31:16];
  assign segment[0] = | in[15:0];
  assign zero_0 = segment[0] ? {2'b00, nets[3:0]} :
      (segment[1] ? {2'b01, nets[7:4]} : 
      (segment[2] ? {2'b10, nets[11:8]} :
      (segment[3] ? {2'b11, nets[15:12]} : 6'b0)));

endmodule
