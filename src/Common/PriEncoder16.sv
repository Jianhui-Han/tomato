module PriEncoder16(
  input   logic [15:0]      in,
  output  logic [3:0]       out
);

  logic [7:0]               nets;
  logic [3:0]               segment;

  PriEncoder4 U3(in[15:12], nets[7:6]);
  PriEncoder4 U2(in[11:8], nets[5:4]);
  PriEncoder4 U1(in[7:4], nets[3:2]);
  PriEncoder4 U0(in[3:0], nets[1:0]);

  assign segment[3] = | in[15:12];
  assign segment[2] = | in[11:8];
  assign segment[1] = | in[7:4];
  assign segment[0] = | in[3:0];
  assign out = segment[0] ? {2'b00, nets[1:0]} :
      (segment[1] ? {2'b01, nets[3:2]} : 
      (segment[2] ? {2'b10, nets[5:4]} :
      (segment[3] ? {2'b11, nets[7:6]} : 4'b0)));

endmodule
