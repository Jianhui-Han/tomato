`include "Configs.svh"

module IssueQueue(
  input   logic                         clk,
  input   logic                         rst_n,
  // Dispatch & ROB
  output  logic                         dispatch_ready,
  input   logic                         dispatch_valid,
  input   logic [`PHY_REGS_B2-1:0]      dispatch_rd,
  input   logic [`PHY_REGS_B2-1:0]      dispatch_rs1,
  input   logic [`PHY_REGS_B2-1:0]      dispatch_rs2,
  input   logic [`DEC_INFO_BITS-1:0]    dispatch_info,
  input   logic [`ROB_DEPTH_B2:0]       rob_tag,
  input   logic                         dispatch_rs1_p,
  input   logic                         dispatch_rs2_p,
  // Execute & RegFile
  input   logic                         execute_ready,
  output  logic                         execute_valid,
  output  logic [`DEC_INFO_BITS-1:0]    execute_info,
  output  logic [`PHY_REGS_B2-1:0]      regfile_rd,
  output  logic [`PHY_REGS_B2-1:0]      regfile_rs1,
  output  logic [`PHY_REGS_B2-1:0]      regfile_rs2,
  output  logic [`ROB_DEPTH_B2:0]       execute_tag,
  input   logic [`PHY_REGS_B2-1:0]      regfile_wd_0,
  input   logic [`PHY_REGS_B2-1:0]      regfile_wd_1
);

  logic [`ISS_QUE_DEP]                          metadata_valid;
  logic [`ISS_QUE_DEP-1:0][`PHY_REGS_B2-1:0]    metadata_rd;
  logic [`ISS_QUE_DEP-1:0][`PHY_REGS_B2-1:0]    metadata_rs1;
  logic [`ISS_QUE_DEP-1:0][`PHY_REGS_B2-1:0]    metadata_rs2;
  logic [`ISS_QUE_DEP-1:0][`DEC_INFO_BITS-1:0]  metadata_info;
  logic [`ISS_QUE_DEP-1:0][`ROB_DEPTH_B2:0]     metadata_tag;
  logic [`ISS_QUE_DEP]                          metadata_rs1_p;
  logic [`ISS_QUE_DEP]                          metadata_rs2_p;
  logic [`ISS_QUE_DEP]                          metadata_ready;

  logic [`ISS_QUE_DEP_B2-1:0]   candidate;
  
  logic [`ISS_QUE_DEP_B2:0]     cnt;

  PriEncoder64 encoder(
    .in     (metadata_ready   ),
    .out    (candidate        )
  );

  assign dispatch_ready = cnt == `ISS_QUE_DEP;
  assign execute_valid = ~((candidate == 0) && (~metadata_ready[0]));
  assign metadata_ready = metadata_valid & metadata_rs1_p & metadata_rs2_p;
  
  always @ (posedge clk or negedge rst_n) begin
    if (!rst_n) begin
      for (int i=0; i<`ISS_QUE_DEP; i=i+1) begin
        metadata_valid[i] <= 1'b0;
        metadata_rd[i] <= {(PHY_REGS_B2){1'b0}};
        metadata_rs1[i] <= {(PHY_REGS_B2){1'b0}};
        metadata_rs2[i] <= {(PHY_REGS_B2){1'b0}};
        metadata_info[i] <= {(DEC_INFO_BITS){1'b0}};
        metadata_tag[i] <= {(ROB_DEPTH_B2){1'b0}};
        metadata_rs1_p[i] <= 1'b0;
        metadata_rs2_p[i] <= 1'b0;
        cnt <= 'd0;
      end
    end
    else begin
      // move forward
      for (int i=0; i<`ISS_QUE_DEP-1; i=i+1) begin
        if (!metadata_valid[i]) begin
          metadata_valid[i] <= metadata_valid[i+1];
          metadata_rd[i] <= metadata_rd[i+1];
          metadata_rs1[i] <= metadata_rs1[i+1];
          metadata_rs2[i] <= metadata_rs2[i+1];
          metadata_info[i] <= metadata_info[i+1];
          metadata_tag[i] <= metadata_tag[i+1];
          metadata_rs1_p[i] <= metadata_rs1_p[i+1] |
              (metadata_rs1[i+1] == regfile_wd_0) |
              (metadata_rs1[i+1] == regfile_wd_1);
          metadata_rs2_p[i] <= metadata_rs2_p[i+1] |
              (metadata_rs2[i+1] == regfile_wd_0) |
              (metadata_rs2[i+1] == regfile_wd_1);
        end
      end
      // fill in
      if (dispatch_ready && dispatch_valid) begin
        metadata_valid <= 1'b1;
        metadata_rd[`ISS_QUE_DEP-1] <= dispatch_rd;
        metadata_rs1[`ISS_QUE_DEP-1] <= dispatch_rs1;
        metadata_rs2[`ISS_QUE_DEP-1] <= dispatch_rs2;
        metadata_info[`ISS_QUE_DEP-1] <= dispatch_info;
        metadata_tag[`ISS_QUE_DEP-1] <= rob_tag;
        metadata_rs1_p[i] <= dispatch_rs1_p;
        metadata_rs2_p[i] <= dispatch_rs2_p;
        cnt <= cnt + 1;
      end
      // issue
      if (execute_ready && execute_valid) begin
        metadata_valid[candidate] <= 1'b0;
        cnt <= cnt - 1;
      end
    end
  end

endmodule
