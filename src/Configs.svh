`ifndef __CONFIGS__
`define __CONFIGS__

// Fetch

// Decode
`define LOG_REGS        32
`define LOG_REGS_B2     $clog2(`LOG_REGS)
`define PHY_REGS        64
`define PHY_REGS_B2     $clog2(`PHY_REGS)
`define DEC_INFO_BITS   12  

// Dispatch
`define ROB_DEPTH       16
`define ROB_DEPTH_B2    $clog2(`ROB_DEPTH)
`define ROB_WIDTH       (2+`PHY_REGS_B2)

// Issue
`define ISS_QUE_DEP     8
`define ISS_QUE_DEP_B2  $clog2(`ISS_QUE_DEP)  

`endif