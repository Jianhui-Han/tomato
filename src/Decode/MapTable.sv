module MapTable(
  input   logic             clk,
  input   logic             rst_n,
  input   logic             req_valid,
  // lookup
  input   logic [4:0]       log_rd_0,
  input   logic [4:0]       log_rs1_0,
  input   logic [4:0]       log_rs2_0,
  input   logic [4:0]       log_rd_1,
  input   logic [4:0]       log_rs1_1,
  input   logic [4:0]       log_rs2_1,
  output  logic [5:0]       phy_rs1_0,
  output  logic [5:0]       phy_rs2_0,
  output  logic [5:0]       phy_stale_0,
  output  logic [5:0]       phy_rs1_1,
  output  logic [5:0]       phy_rs2_1,
  output  logic [5:0]       phy_stale_1,
  // update
  input   logic [5:0]       phy_rd_0,
  input   logic [5:0]       phy_rd_1
);

  logic [31:0][5:0]         metadata;
  integer                   i;

  // lookup
  assign phy_rs1_0 = metadata[log_rs1_0];
  assign phy_rs2_0 = metadata[log_rs2_0];
  assign phy_stale_0 = metadata[log_rd_0];
  assign phy_rs1_1 = metadata[log_rs1_1];
  assign phy_rs2_1 = metadata[log_rs2_1];
  assign phy_stale_1 = metadata[log_rd_1];

  always @ (posedge clk or negedge rst_n) begin
    if (!rst_n) begin
      for (i=0; i<32; i=i+1)
        metadata[i] = i;
    end
    else begin
      if (req_valid) begin
        // update
        metadata[log_rd_0] <= phy_rd_0;
        metadata[log_rd_1] <= phy_rd_1;
      end    
    end 
  end

endmodule
