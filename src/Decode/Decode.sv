module Decode(
  input   logic                             clk,
  input   logic                             rst_n,
  // fetch
  input   logic                             fetch_inst_valid,
  output  logic                             fetch_inst_ready,
  input   logic [31:0]                      fetch_inst_data_0,
  input   logic [31:0]                      fetch_inst_data_1,
  input   logic [31:0]                      fetch_pc,
  // dispatch
  output  logic                             dispatch_valid,
  input   logic                             dispatch_ready,
  output  logic [5:0]                       dispatch_rd_0,
  output  logic [5:0]                       dispatch_rs1_0,
  output  logic [5:0]                       dispatch_rs2_0,
  output  logic [5:0]                       dispatch_rd_1,
  output  logic [5:0]                       dispatch_rs1_1,
  output  logic [5:0]                       dispatch_rs2_1,
  output  logic [31:0]                      dispatch_pc,
  // commit
  input   logic                             commit_valid_0,
  input   logic [5:0]                       commit_stale_0,
  input   logic                             commit_valid_1,
  input   logic [5:0]                       commit_stale_1
);

  logic [5:0]     phy_rs1_0, phy_rs2_0, phy_rs1_1, phy_rs2_1;
  logic [5:0]     phy_stale_0, phy_stale_1, phy_rd_0, phy_rd_1;
  logic           free_list_empty

  logic [5:0]     phy_stale_1_t, phy_rs1_1_t, phy_rs2_1_t;

  MapTable map_table(
    .clk                (clk),
    .rst_n              (rst_n),
    .req_valid          (fetch_inst_valid),
    .log_rd_0           (fetch_inst_data_0[11:7]),
    .log_rs1_0          (fetch_inst_data_0[19:15]),
    .log_rs2_0          (fetch_inst_data_0[24:20]),
    .log_rd_1           (fetch_inst_data_1[11:7]),
    .log_rs1_1          (fetch_inst_data_1[19:15]),
    .log_rs2_1          (fetch_inst_data_1[24:20]),
    .phy_rs1_0          (phy_rs1_0),
    .phy_rs2_0          (phy_rs2_0),
    .phy_stale_0        (phy_stale_0),
    .phy_rs1_1          (phy_rs1_1_t),
    .phy_rs2_1          (phy_rs2_1_t),
    .phy_stale_1        (phy_stale_1_t),
    .phy_rd_0           (phy_rd_0),
    .phy_rd_1           (phy_rd_1)
  );

  FreeList free_list(
    .clk                (clk),
    .rst_n              (rst_n),
    .empty              (free_list_empty),
    .lookup_valid       (fetch_inst_valid),
    .lookup_rd_0        (phy_rd_0),
    .lookup_rd_1        (phy_rd_1),
    .update_valid_0     (commit_valid_0),
    .update_rd_0        (commit_stale_0),
    .update_valid_1     (commit_valid_1),
    .update_rd_1        (commit_stale_1)
  );

  // bypass
  assign phy_stale_1 = (fetch_inst_data_0[11:7] == fetch_inst_data_1[11:7]) ?
      phy_rd_0 : phy_stale_1_t;
  assign phy_rs1_1 = (fetch_inst_data_0[11:7] == fetch_inst_data_1[19:15]) ?
      phy_rd_0 : phy_rs1_1_t;
  assign phy_rs2_1 = (fetch_inst_data_0[11:8] == fetch_inst_data_1[24:20]) ?
      phy_rd_0 : phy_rs2_1_t;

  // output
  assign dispatch_rd_0 = phy_stale_0;
  assign dispatch_rs1_0 = phy_rs1_0;
  assign dispatch_rs2_0 = phy_rs2_0;
  assign dispatch_rd_1 = phy_stale_1;
  assign dispatch_rs1_1 = phy_rs1_1;
  assign dispatch_rs2_1 = phy_rs2_1;

  always @ (posedge clk or negedge rst_n) begin
    if (!rst_n)
      dispatch_pc <= 32'b0;
    else
      dispatch_pc <= fetch_pc;
  end

endmodule
