module FreeList(
  input   logic                     clk,
  input   logic                     rst_n,
  output  logic                     empty,
  // lookup
  input   logic                     lookup_valid,
  output  logic [`PHY_REGS_B2-1:0]  lookup_rd_0,
  output  logic [`PHY_REGS_B2-1:0]  lookup_rd_1
  // update
  input   logic                     update_valid_0,
  input   logic [`PHY_REGS_B2-1:0]  update_rd_0,
  input   logic                     update_valid_1,
  input   logic [`PHY_REGS_B2-1:0]  update_rd_1
);

  logic [`PHY_REGS-1:0]     metadata;
  logic [`PHY_REGS-1:0]     metadata_re;
  logic [`PHY_REGS_B2:0]    cnt;

  PriEncoder encoder_lsb(
    .in       (metadata     ),
    .out      (lookup_rd_0  )
  );

  PriEncoder encoder_msb(
    .in       (metadata_re  ),
    .out      (lookup_rd_1  )
  );
  
  genvar i;
  generate
    for (i=0; i<`PHY_REGS; i=i+1) begin : reverse
      assign metadata_re[i] = metadata[`PHY_REGS-1-i];
    end
  endgenerate

  assign empty = (cnt == (`PHY_REGS_B2-1)) || (cnt == (`PHY_REGS_B2-2));

  always @ (posedge clk or negedge rst_n) begin
    if (!rst_n) begin
      metadata <= {(`PHY_REGS-32){1'b1}, 32'h00000000};
      cnt <= 'd32;
    end
    else begin
      // lookup_rd_X and update_rd_X should never be the same, logically.
      if (lookup_valid) begin
        metadata[lookup_rd_0] <= 1'b0;
        metadata[lookup_rd_1] <= 1'b0;
      end
      if (updata_valid_0)
        metadata[update_rd_0] <= 1'b1;
      if (update_valid_1)
        metadata[update_rd_1] <= 1'b1;
      // for empty logic
      case({lookup_valid, update_valid_0, update_valid_1})
        3'b000: cnt <= cnt;
        3'b001: cnt <= cnt + 1;
        3'b010: cnt <= cnt + 1;
        3'b011: cnt <= cnt + 2;
        3'b100: cnt <= cnt - 2;
        3'b101: cnt <= cnt - 1;
        3'b110: cnt <= cnt - 1;
        3'b111: cnt <= cnt;
      endcase
  end

endmodule
