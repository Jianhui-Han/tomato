module Fetch(
  input   logic                           clk,
  input   logic                           rst_n,
  // icache
  output  logic                           icache_req_valid,
  input   logic                           icache_req_ready,
  output  logic [31:0]                    icache_req_addr,
  input   logic                           icache_resp_valid,
  output  logic                           icache_resp_ready,
  input   logic [63:0]                    icache_resp_data,
  // decode
  output  logic                           decode_inst_valid,
  input   logic                           decode_inst_ready,
  output  logic [31:0]                    decode_inst_data_0,
  output  logic [31:0]                    decode_inst_data_1    
);

  // pc logic & icache interface
  logic [31:0]                        pc, pc_next;

  assign icache_req_valid = decode_inst_ready;
  assign icache_resp_ready = 1'b1;
  assign icache_req_addr = pc;
  assign pc_next = pc + 8;

  always @ (posedge clk or negedge rst_n) begin
    if (!rst_n)
      pc <= 32'b0;
    else
      if (icache_req_ready && icache_req_valid)
        pc <= pc_next;
  end

  // decode interface
  always @ (posedge clk or negedge rst_n) begin
    if (!rst_n) begin
      decode_inst_valid <= 1'b0;
      decode_inst_data_0 <= 32'b0;
      decode_inst_data_1 <= 32'b0;
    end
    else begin
      decode_inst_valid <= icache_resp_valid;
      decode_inst_data_0 <= icache_resp_data[31:0];
      decode_inst_data_1 <= icache_resp_data[63:32];
    end
  end
  
endmodule
