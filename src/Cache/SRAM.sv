module SRAM #(
  parameter int unsigned DEPTH        = 64,
  parameter int unsigned DATA_WIDTH   = 32,
  parameter int unsigned ADDR_WIDTH   = $clog2(DEPTH)
) (
  input   logic                       clk_a,
  input   logic                       cen_a,
  input   logic [ADDR_WIDTH-1:0]      addr_a,
  output  logic [DATA_WIDTH-1:0]      data_a,
  input   logic                       clk_b,
  input   logic                       cen_b,
  input   logic [ADDR_WIDTH-1:0]      addr_b,
  input   logic [DATA_WIDTH-1:0]      data_b
);

  logic [DATA_WIDTH-1:0]  array[DEPTH-1:0];

`ifdef MEM_INIT_ZERO
  integer i;
  initial begin
      for (i=0; i<DEPTH; i=i+1)
          array[i] = 0;
  end
`endif

  always @ (posedge clk_a) begin
    if (cen_a)
      data_a <= array[addr_a];
  end

  always @ (posedge clk_b) begin
    if (cen_b)
      array[addr_b] <= data_b;
  end

endmodule
