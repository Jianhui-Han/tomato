module ICache (
  input   logic                           clk,
  input   logic                           rst_n,
  // core side
  input   logic                           req_valid,
  output  logic                           req_ready,
  input   logic [31:0]                    req_addr,
  output  logic                           resp_valid,
  input   logic                           resp_ready,
  output  logic [63:0]                    resp_data,
  // mem side
  output  logic                           axi_m_awid,
  output  logic [31:0]                    axi_m_awaddr,
  output  logic [7:0]                     axi_m_awlen,
  output  logic [2:0]                     axi_m_awsize,
  output  logic [1:0]                     axi_m_awburst,
  output  logic                           axi_m_awlock,
  output  logic [3:0]                     axi_m_awcache,
  output  logic [2:0]                     axi_m_awprot,
  output  logic [3:0]                     axi_m_awqos,
  output  logic [3:0]                     axi_m_awregion,
  output  logic                           axi_m_awuser,
  output  logic                           axi_m_awvalid,
  input   logic                           axi_m_awready,
  output  logic [127:0]                   axi_m_wdata,
  output  logic [15:0]                    axi_m_wstrb,
  output  logic                           axi_m_wlast,
  output  logic                           axi_m_wuser,
  output  logic                           axi_m_wvalid,
  input   logic                           axi_m_wready,
  input   logic                           axi_m_bid,
  input   logic [1:0]                     axi_m_bresp,
  input   logic                           axi_m_buser,
  input   logic                           axi_m_bvalid,
  output  logic                           axi_m_bready,
  output  logic                           axi_m_arid,
  output  logic [31:0]                    axi_m_araddr,
  output  logic [7:0]                     axi_m_arlen,
  output  logic [2:0]                     axi_m_arsize,
  output  logic [1:0]                     axi_m_arburst,
  output  logic                           axi_m_arlock,
  output  logic [3:0]                     axi_m_arcache,
  output  logic [2:0]                     axi_m_arprot,
  output  logic [3:0]                     axi_m_arqos,
  output  logic [3:0]                     axi_m_arregion,
  output  logic                           axi_m_aruser,
  output  logic                           axi_m_arvalid,
  input   logic                           axi_m_arready,
  input   logic                           axi_m_rid,
  input   logic [127:0]                   axi_m_rdata,
  input   logic [1:0]                     axi_m_rresp,
  input   logic                           axi_m_rlast,
  input   logic                           axi_m_ruser,
  input   logic                           axi_m_rvalid,
  output  logic                           axi_m_rready
);

  localparam S_INIT = 0;
  localparam S_IDLE = 1;
  localparam S_BUSY = 2;

  logic [1:0]                         current_state;
  logic [1:0]                         next_state;

  logic                               fetch_miss, fetch_hit;
  logic [3:0]                         fetch_hit_way;
  logic [1:0]                         fetch_hit_binary;

  logic                               req_fire;
  logic                               do_init;
  logic                               init_done;
  logic                               fill_done;
  logic [31:0]                        req_addr_fire;
  logic                               req_fire_d;

  logic [6:0]                         tag_array_addr_a;
  logic [6:0]                         data_array_addr_a;
  logic [19:0]                        tag_array_data_a    [3:0];
  logic [511:0]                       data_array_data_a   [3:0];
  logic [19:0]                        tag_array_data_b;
  logic                               tag_array_cen_b     [3:0];
  logic [511:0]                       data_array_data_b;
  logic                               data_array_cen_b    [3:0];
  logic [6:0]                         tag_array_addr_b;
  logic [6:0]                         data_array_addr_b;

  logic [18:0]                        input_tag;
  logic [8:0]                         resp_sel_base;
  logic [63:0]                        resp_data_comb;

  logic [6:0]                         init_cnt;

  logic [3:0]                         victim_way;
  logic [6:0]                         victim_set;
  logic [18:0]                        victim_tag;

  logic [511:0]                       fill_buffer;

  integer                             t;


  // tag & data array
  genvar i;
  generate
    for (i=0; i<4; i=i+1) begin : arrays
      SRAM #(
        .DEPTH        (128),
        .DATA_WIDTH   (20)
      ) tag_array (
        .clk_a        (clk),
        .cen_a        (req_valid),
        .addr_a       (tag_array_addr_a),
        .data_a       (tag_array_data_a[i]),
        .clk_b        (clk),
        .cen_b        (tag_array_cen_b[i]),
        .addr_b       (tag_array_addr_b),
        .data_b       (tag_array_data_b)
      );

      SRAM #(
        .DEPTH        (128),
        .DATA_WIDTH   (512)
      ) data_array (
        .clk_a        (clk),
        .cen_a        (req_valid),
        .addr_a       (data_array_addr_a),
        .data_a       (data_array_data_a[i]),
        .clk_b        (clk),
        .cen_b        (data_array_cen_b[i]),
        .addr_b       (data_array_addr_b),
        .data_b       (data_array_data_b)
      );
    end
  endgenerate

  assign tag_array_addr_a = req_addr[12:6];
  assign data_array_addr_a = tag_array_addr_a;
  generate
    for (i=0; i<4; i=i+1) begin : array_cen
      assign tag_array_cen_b[i] = do_init || (victim_way[i] && fill_done);
      assign data_array_cen_b[i] = tag_array_cen_b[i]; 
    end
  endgenerate
  assign tag_array_addr_b = do_init ? init_cnt : victim_set;
  assign data_array_addr_b = tag_array_addr_b;
  assign tag_array_data_b = do_init ? 20'b0 : {1'b1, victim_tag};
  assign data_array_data_b = do_init ? 512'b0 : fill_buffer;


  // matching & selecting
  assign input_tag = req_addr[31:13];
  generate
    for (i=0; i<4; i=i+1) begin : hit_way
      assign fetch_hit_way[i] = tag_array_data_a[i][19] &&
          (tag_array_data_a[i][18:0] == input_tag);
    end
  endgenerate
  assign fetch_hit = (|fetch_hit_way) && req_fire_d;
  assign fetch_miss = (&(~fetch_hit_way)) && req_fire_d;
  assign fetch_hit_binary = fetch_hit_way[0] ? 2'd0 :
      (fetch_hit_way[1] ? 2'd1 : (fetch_hit_way[2] ? 2'd2 : 2'd3));

  assign resp_sel_base = req_addr_fire[5:0] << 3;
  assign resp_data_comb = fill_done ? fill_buffer[resp_sel_base +: 64] :
      data_array_data_a[fetch_hit_binary][resp_sel_base +: 64];


  // state machine
  assign req_fire = req_ready && req_valid;

  always @ (posedge clk or negedge rst_n) begin
    if (!rst_n) begin
      req_addr_fire <= 32'b0;
      req_fire_d <= 1'b0;
    end
    else
      if (req_fire)
        req_addr_fire <= req_addr;
      req_fire_d <= req_fire;
  end

  always @ (posedge clk or negedge rst_n) begin
    if (!rst_n)
      current_state <= S_INIT;
    else
      current_state <= next_state;
  end

  always @ (*) begin
    case (current_state)
      S_INIT:
        if (init_done)
          next_state = S_IDLE;
        else
          next_state = S_INIT;
      S_IDLE:
        if (fetch_miss)
          next_state = S_BUSY;
        else
          next_state = S_IDLE;
      S_BUSY:
        if (fill_done)
          next_state = S_IDLE;
        else
          next_state = S_BUSY;
      default:
        next_state = S_INIT;
    endcase
  end

  always @ (*) begin
    case (current_state)
      S_INIT: begin
        req_ready = 1'b0;
        do_init = 1'b1;
        resp_valid = 1'b0;
        resp_data = 64'b0;
      end
      S_IDLE: begin
        req_ready = ~fetch_miss;
        do_init = 1'b0;
        resp_valid = fetch_hit;
        resp_data = resp_data_comb;
      end
      S_BUSY: begin
        req_ready = 1'b0;
        do_init = 1'b0;
        resp_valid = fill_done;
        resp_data = resp_data_comb;
      end
      default: begin
        req_ready = 1'b0;
        do_init = 1'b0;
        resp_valid = 1'b0;
        resp_data = 64'b0;
      end
    endcase
  end


  // init logic
  always @ (posedge clk or negedge rst_n) begin
    if (!rst_n) begin
      init_done <= 1'b0;
      init_cnt <= 7'b0;
    end
    else begin
      if (do_init)
        init_cnt <= init_cnt + 1;
      if (init_cnt == 7'h7f)
        init_done <= 1'b1;
    end
  end


  // replacement logic
  TreePLRU repl(
    .clk                (clk),
    .rst_n              (rst_n),
    .req_addr_fire      (req_addr_fire),
    .fetch_hit          (fetch_hit),
    .fetch_miss         (fetch_miss),
    .fetch_hit_way      (fetch_hit_way),
    .victim_way         (victim_way),
    .victim_set         (victim_set),
    .victim_tag         (victim_tag)
  );


  // bus interface
  // ar channel
  assign axi_m_arid = 1'b0;
  assign axi_m_arlen = 7'b0000011;
  assign axi_m_arsize = 3'b111;
  assign axi_m_arburst = 2'b01;
  assign axi_m_arlock = 2'b00;
  assign axi_m_arcache = 4'b0000;
  assign axi_m_arprot = 3'b000;
  assign axi_m_arqos = 4'b0000;
  assign axi_m_arregion = 4'b0000;
  assign axi_m_aruser = 1'b0;

  always @ (posedge clk or negedge rst_n) begin
    if (!rst_n) begin
      axi_m_arvalid <= 1'b0;
      axi_m_araddr <= 128'b0;
    end
    else begin
      if (fetch_miss) begin
        axi_m_arvalid <= 1'b1;
        axi_m_araddr <= req_addr_fire;
      end
      else if (axi_m_arvalid && axi_m_arready) begin
        axi_m_arvalid <= 1'b0;
        axi_m_araddr <= 128'b0;
      end
    end
  end

  // r channel
  always @ (posedge clk or negedge rst_n) begin
    if (!rst_n) begin
      axi_m_rready <= 1'b1;
      fill_buffer <= 512'b0;
      fill_done <= 1'b0;
    end
    else begin
      if (axi_m_rready && axi_m_rvalid) begin
        if (axi_m_rlast) begin
          axi_m_rready <= 1'b0;
          fill_done <= 1'b1;
        end
        fill_buffer <= (fill_buffer >> 128) | (axi_m_rdata << 384); 
      end
      else if (fill_done) begin
        axi_m_rready <= 1'b1;
        fill_buffer <= 512'b0;
        fill_done <= 1'b0;
      end
    end
  end

endmodule
