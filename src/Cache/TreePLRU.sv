// see https://people.cs.clemson.edu/~mark/464/p_lru.txt
module TreePLRU(
  input   logic                           clk,
  input   logic                           rst_n,
  input   logic [31:0]                    req_addr_fire,
  input   logic                           fetch_hit,
  input   logic                           fetch_miss,
  input   logic [3:0]                     fetch_hit_way,
  output  logic [3:0]                     victim_way,
  output  logic [6:0]                     victim_set,
  output  logic [18:0]                    victim_tag
);

  logic [3:0]                         metadata          [127:0];
  
  integer t;
  

  // replacement logic
  always @ (posedge clk or negedge rst_n) begin
    if (!rst_n) begin
      victim_set <= 7'b0;
      victim_tag <= 19'b0;
      victim_way <= 4'b0000;
      for (t=0; t<128; t=t+1)
        metadata[t] <= 3'b000;
    end
    else begin
      if (fetch_miss) begin
        victim_set <= req_addr_fire[12:6];
        victim_tag <= req_addr_fire[31:13];
        case (metadata[req_addr_fire[12:6]])
          3'b000, 3'b001: begin
            victim_way <= 4'b0001;
            metadata[req_addr_fire[12:6]] <= {2'b11, metadata[req_addr_fire[12:6]][0]};
          end
          3'b010, 3'b011: begin
            victim_way <= 4'b0010;
            metadata[req_addr_fire[12:6]] <= {2'b10, metadata[req_addr_fire[12:6]][0]};
          end
          3'b100, 3'b110: begin
            victim_way <= 4'b0100;
            metadata[req_addr_fire[12:6]] <= {1'b0, metadata[req_addr_fire[12:6]][1], 1'b1};
          end
          3'b101, 3'b111: begin
            victim_way <= 4'b1000;
            metadata[req_addr_fire[12:6]] <= {1'b0, metadata[req_addr_fire[12:6]][1], 1'b0};
          end
        endcase
      end
      else if (fetch_hit) begin
        case (fetch_hit_way)
          4'b0001:
            metadata[req_addr_fire[12:6]] <= {2'b11, metadata[req_addr_fire[12:6]][0]};
          4'b0010:
            metadata[req_addr_fire[12:6]] <= {2'b10, metadata[req_addr_fire[12:6]][0]};
          4'b0100:
            metadata[req_addr_fire[12:6]] <= {1'b0, metadata[req_addr_fire[12:6]][1], 1'b1};
          4'b1000:
            metadata[req_addr_fire[12:6]] <= {1'b0, metadata[req_addr_fire[12:6]][1], 1'b0};
        endcase
      end
    end
  end

endmodule
