#! /bin/bash -v

PROJ_DIR=$PWD

VCS=vcs
VCS_FLAG="-sverilog -full64"
SIMV=simv

if [ $1 == "ICache" ]; then
  SRC="$PROJ_DIR/test/ICache_test.sv \
			$PROJ_DIR/src/ICache.sv \
  		$PROJ_DIR/test/utils/axi_ram.v \
  		$PROJ_DIR/src/SRAM.sv \
      $PROJ_DIR/src/TreePLRU.sv"
elif [ $1 == "Fetch" ]; then
  SRC="$PROJ_DIR/test/Fetch_test.sv \
			$PROJ_DIR/src/Fetch.sv"
fi

rm -rf work
mkdir -p work
cd work
$PROJ_DIR/scripts/utils/gen_init.py
$VCS $VCS_FLAG $SRC
./$SIMV
