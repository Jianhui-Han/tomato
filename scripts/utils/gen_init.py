#! /usr/bin/env python3

num = 8192
str = "0123456789ABCDEF0123456789ABCDEF"

with open("mem_init.dat", "w") as fout:
  for i in range(num):
    fout.write(str)
    fout.write("\n")
    str = str[1:] + str[0]
