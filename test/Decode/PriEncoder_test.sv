module PriEncoder_test;

  logic [63:0]      vector;
  logic [5:0]       zero_0;
  logic [5:0]       zero_1;

  PriEncoder DUT(
    .vector(vector),
    .zero_0(zero_0),
    .zero_1(zero_1)
  );

  initial begin
    vector = 64'b0;
  end

  always #5 vector = vector + 1;

  initial begin
    $dumpfile("PriEncoder_test.vcd");
    $dumpvars(0, PriEncoder_test);
    #100000 $finish;
  end

endmodule
