`timescale 1ns / 1ps

module Fetch_test;

  logic                                   clk;                
  logic                                   rst_n;              
  logic                                   icache_req_valid;   
  logic                                   icache_req_ready;   
  logic [31:0]                            icache_req_addr;    
  logic                                   icache_resp_valid;  
  logic                                   icache_resp_ready;  
  logic [63:0]                            icache_resp_data;   
  logic                                   decode_inst_valid;  
  logic                                   decode_inst_ready;
  logic [1:0][31:0]                       decode_inst_data;

  logic [3:0]                             cnt;
  logic                                   long_latency;
  logic                                   pending;

  Fetch DUT(
    .clk                      (clk),
    .rst_n                    (rst_n),
    .icache_req_valid         (icache_req_valid),
    .icache_req_ready         (icache_req_ready),
    .icache_req_addr          (icache_req_addr),
    .icache_resp_valid        (icache_resp_valid),
    .icache_resp_ready        (icache_resp_ready),
    .icache_resp_data         (icache_resp_data),
    .decode_inst_valid        (decode_inst_valid),
    .decode_inst_ready        (decode_inst_ready),
    .decode_inst_data         (decode_inst_data)
  );

  initial begin
    clk = 0;
    rst_n = 0;
    #20
    rst_n = 1;
  end
  always #5 clk = ~clk;

  always @ (posedge clk or negedge rst_n) begin
    if (!rst_n) begin
      icache_resp_data = 64'h0123456789ABCDEF;
    end
    else begin
      if (icache_resp_ready && icache_resp_valid)
        icache_resp_data = {icache_resp_data[3:0], icache_resp_data[63:4]};
    end
  end

  always @ (posedge clk or negedge rst_n) begin
    if (!rst_n) begin
      icache_resp_valid <= 1'b0;
      pending <= 1'b0;
    end
    else begin
      if (long_latency) begin
        if (icache_req_valid)
          pending <= 1'b1;
        if (cnt == 4'hf && pending) begin
          pending <= 1'b0;
          icache_resp_valid <= 1'b1;
        end
      end
      else begin
        icache_resp_valid <= icache_req_valid;
      end
    end
  end

  always @ (posedge clk or negedge rst_n) begin
    if (!rst_n)
      cnt <= 4'b0000;
    else
      cnt <= cnt + 1;
  end

  initial begin
    long_latency = 1'b0;
    #50000
    long_latency = 1'b1;
  end

  initial begin
    decode_inst_ready = 1'b1;
    #1000
    decode_inst_ready = 1'b0;
    #1000
    decode_inst_ready = 1'b1;
  end

  initial begin
    $dumpfile("Fetch_test.vcd");
    $dumpvars(0, Fetch_test);
    #100000 $finish;
  end

endmodule
