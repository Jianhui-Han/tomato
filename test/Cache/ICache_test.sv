`timescale 1ns / 1ps

module ICache_test;

  logic   clk;
  logic   rst_n;

  logic                           req_valid;
  logic                           req_ready;
  logic [31:0]                    req_addr;
  logic                           resp_valid;
  logic                           resp_ready;
  logic [63:0]                    resp_data;
  logic                           axi_m_awid;
  logic [31:0]                    axi_m_awaddr;
  logic [7:0]                     axi_m_awlen;
  logic [2:0]                     axi_m_awsize;
  logic [1:0]                     axi_m_awburst;
  logic                           axi_m_awlock;
  logic [3:0]                     axi_m_awcache;
  logic [2:0]                     axi_m_awprot;
  logic [3:0]                     axi_m_awqos;
  logic [3:0]                     axi_m_awregion;
  logic                           axi_m_awuser;
  logic                           axi_m_awvalid;
  logic                           axi_m_awready;
  logic [127:0]                   axi_m_wdata;
  logic [15:0]                    axi_m_wstrb;
  logic                           axi_m_wlast;
  logic                           axi_m_wuser;
  logic                           axi_m_wvalid;
  logic                           axi_m_wready;
  logic                           axi_m_bid;
  logic [1:0]                     axi_m_bresp;
  logic                           axi_m_buser;
  logic                           axi_m_bvalid;
  logic                           axi_m_bready;
  logic                           axi_m_arid;
  logic [31:0]                    axi_m_araddr;
  logic [7:0]                     axi_m_arlen;
  logic [2:0]                     axi_m_arsize;
  logic [1:0]                     axi_m_arburst;
  logic                           axi_m_arlock;
  logic [3:0]                     axi_m_arcache;
  logic [2:0]                     axi_m_arprot;
  logic [3:0]                     axi_m_arqos;
  logic [3:0]                     axi_m_arregion;
  logic                           axi_m_aruser;
  logic                           axi_m_arvalid;
  logic                           axi_m_arready;
  logic                           axi_m_rid;
  logic [127:0]                   axi_m_rdata;
  logic [1:0]                     axi_m_rresp;
  logic                           axi_m_rlast;
  logic                           axi_m_ruser;
  logic                           axi_rvalid;
  logic                           axi_rready;

  ICache DUT(
    .clk                (clk),
    .rst_n              (rst_n),
    .req_valid          (req_valid),
    .req_ready          (req_ready),
    .req_addr           (req_addr),
    .resp_valid         (resp_valid),
    .resp_ready         (resp_ready),
    .resp_data          (resp_data),
    .axi_m_awid         (axi_m_awid),
    .axi_m_awaddr       (axi_m_awaddr),
    .axi_m_awlen        (axi_m_awlen),
    .axi_m_awsize       (axi_m_awsize),
    .axi_m_awburst      (axi_m_awburst),
    .axi_m_awlock       (axi_m_awlock),
    .axi_m_awcache      (axi_m_awcache),
    .axi_m_awprot       (axi_m_awprot),
    .axi_m_awqos        (axi_m_awqos),
    .axi_m_awregion     (axi_m_awregion),
    .axi_m_awuser       (axi_m_awuser),
    .axi_m_awvalid      (axi_m_awvalid),
    .axi_m_awready      (axi_m_awready),
    .axi_m_wdata        (axi_m_wdata),
    .axi_m_wstrb        (axi_m_wstrb),
    .axi_m_wlast        (axi_m_wlast),
    .axi_m_wuser        (axi_m_wuser),
    .axi_m_wvalid       (axi_m_wvalid),
    .axi_m_wready       (axi_m_wready),
    .axi_m_bid          (axi_m_bid),
    .axi_m_bresp        (axi_m_bresp),
    .axi_m_buser        (axi_m_buser),
    .axi_m_bvalid       (axi_m_bvalid),
    .axi_m_bready       (axi_m_bready),
    .axi_m_arid         (axi_m_arid),
    .axi_m_araddr       (axi_m_araddr),
    .axi_m_arlen        (axi_m_arlen),
    .axi_m_arsize       (axi_m_arsize),
    .axi_m_arburst      (axi_m_arburst),
    .axi_m_arlock       (axi_m_arlock),
    .axi_m_arcache      (axi_m_arcache),
    .axi_m_arprot       (axi_m_arprot),
    .axi_m_arqos        (axi_m_arqos),
    .axi_m_arregion     (axi_m_arregion),
    .axi_m_aruser       (axi_m_aruser),
    .axi_m_arvalid      (axi_m_arvalid),
    .axi_m_arready      (axi_m_arready),
    .axi_m_rid          (axi_m_rid),
    .axi_m_rdata        (axi_m_rdata),
    .axi_m_rresp        (axi_m_rresp),
    .axi_m_rlast        (axi_m_rlast),
    .axi_m_ruser        (axi_m_ruser),
    .axi_m_rvalid       (axi_m_rvalid),
    .axi_m_rready       (axi_m_rready)
  );

  axi_ram #(
    .DATA_WIDTH         (128),
    .ADDR_WIDTH         (20),
    .STRB_WIDTH         (16),
    .ID_WIDTH           (1),
    .PIPELINE_OUTPUT    (0)
  ) axi_mem (
    .clk              (clk),
    .rst              (~rst_n),
    .s_axi_awid       (axi_m_awid),
    .s_axi_awaddr     (axi_m_awaddr),
    .s_axi_awlen      (axi_m_awlen),
    .s_axi_awsize     (axi_m_awsize),
    .s_axi_awburst    (axi_m_awburst),
    .s_axi_awlock     (axi_m_awlock),
    .s_axi_awcache    (axi_m_awcache),
    .s_axi_awprot     (axi_m_awprot),
    .s_axi_awvalid    (axi_m_awvalid),
    .s_axi_awready    (axi_m_awready),
    .s_axi_wdata      (axi_m_wdata),
    .s_axi_wstrb      (axi_m_wstrb),
    .s_axi_wlast      (axi_m_wlast),
    .s_axi_wvalid     (axi_m_wvalid),
    .s_axi_wready     (axi_m_wready),
    .s_axi_bid        (axi_m_bid),
    .s_axi_bresp      (axi_m_bresp),
    .s_axi_bvalid     (axi_m_bvalid),
    .s_axi_bready     (axi_m_bready),
    .s_axi_arid       (axi_m_arid),
    .s_axi_araddr     (axi_m_araddr),
    .s_axi_arlen      (axi_m_arlen),
    .s_axi_arsize     (axi_m_arsize),
    .s_axi_arburst    (axi_m_arburst),
    .s_axi_arlock     (axi_m_arlock),
    .s_axi_arcache    (axi_m_arcache),
    .s_axi_arprot     (axi_m_arprot),
    .s_axi_arvalid    (axi_m_arvalid),
    .s_axi_arready    (axi_m_arready),
    .s_axi_rid        (axi_m_rid),
    .s_axi_rdata      (axi_m_rdata),
    .s_axi_rresp      (axi_m_rresp),
    .s_axi_rlast      (axi_m_rlast),
    .s_axi_rvalid     (axi_m_rvalid),
    .s_axi_rready     (axi_m_rready)
  );

  initial begin
    clk = 0;
    rst_n = 0;
    #20
    rst_n = 1;
  end
  always #5 clk = ~clk;

  always @ (posedge clk or negedge rst_n) begin
    if (!rst_n) begin
      req_valid <= 1'b0;
      req_addr <= 32'b0;
    end
    else begin
      req_valid <= 1'b1;
      if (req_valid && req_ready)
        req_addr <= req_addr + 8;
    end
  end

  logic [127:0] mem [8191:0];
  integer i;
  initial begin
    #100
    $readmemh("mem_init.dat", mem);
    $display("%x", mem[0]);
    for (i=0; i<8192; i=i+1)
      axi_mem.mem[i] = mem[i];
    $display("%x", axi_mem.mem[0]);
  end

  initial begin
    $dumpfile("ICache_test.vcd");
    $dumpvars(0, ICache_test);
    #100000 $finish;
  end

  assign resp_ready = 1'b1;

endmodule
